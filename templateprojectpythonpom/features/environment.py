
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager


from webdriver_manager.microsoft import EdgeChromiumDriverManager
from webdriver_manager.firefox import GeckoDriverManager

from templateprojectpythonpom.utilities import ConfigReader

#import sys
#import os
#sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from templateprojectpythonpom.utilities import ConfigReader


def before_scenario(context, driver):
    browser_name = ConfigReader.read_configuration("basic info", "browser")

    if browser_name.__eq__("chrome"):
        chrome_options = Options()
        chrome_options.add_argument('--incognito')  # Inicializa incognito
        chrome_options.add_argument('--start-maximized')  # Maximiza la ventana
        serviceChrome = Service(ChromeDriverManager().install())
        context.driver = webdriver.Chrome(service=serviceChrome, options=chrome_options)
    elif browser_name.__eq__("firefox"):
        firefox_options = Options()
        firefox_options.add_argument('-private')
        firefox_options.add_argument('--start-maximized')
        serviceFirefox = Service(GeckoDriverManager().install())
        context.driver = webdriver.Firefox(service=serviceFirefox)

    elif browser_name.__eq__("edge"):
        edge_options = Options()
        edge_options.add_argument('-inprivate')
        edge_options.add_argument('--start-maximized')
        manager = EdgeChromiumDriverManager()
        serviceEdge = Service(manager.install())
        context.driver = webdriver.Edge(service=serviceEdge)

    #chrome_options = Options()
    #chrome_options.add_argument('--incognito')  # Inicializa incognito
    #chrome_options.add_argument('--start-maximized')  # Maximiza la ventana
    #serviceChrome = Service(ChromeDriverManager().install())
    #context.driver = webdriver.Chrome(service=serviceChrome, options=chrome_options)

    context.driver.get(ConfigReader.read_configuration("basic info","url"))

def after_scenario(context, driver):
    context.driver.quit()

