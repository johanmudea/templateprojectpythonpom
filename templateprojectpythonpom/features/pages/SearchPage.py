from selenium.webdriver.common.by import By

class SearchPage:
    def __init__(self, driver):
        self.driver = driver


    search_box_path = "search"
    search_button_path = "//button[contains(@class,'btn btn-default btn-lg' )]"
    searched_product_name_path = "HP LP3065"
    proper_message_path = "//*[@id='content']/p[2]"

    def enter_search_text(self, text):
        self.driver.find_element(By.NAME, self.search_box_path).send_keys(text)

    def click_on_search_button(self):
        self.driver.find_element(By.XPATH, self.search_button_path).click()

    def display_status_of_searched_product(self):
        return self.driver.find_element(By.LINK_TEXT, self.searched_product_name_path).is_displayed()

    def display_proper_message(self, expected_Warning):
        return self.driver.find_element(By.XPATH, self.proper_message_path).text.__contains__(expected_Warning)

