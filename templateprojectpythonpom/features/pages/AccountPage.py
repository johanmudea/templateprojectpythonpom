from selenium.webdriver.common.by import By


class AccountPage:
    def __init__(self, driver):
        self.driver = driver

    my_account_option_path = "//a[contains(@class,'dropdown-toggle') and contains(@title,'My Account') ]"
    logout_option_message_path = '//*[@id="top-links"]/ul/li[2]/ul/li[5]/a'

    def display_status_of_logout_your_account_information_option(self):
        return self.driver.find_element(By.XPATH, self.logout_option_message_path).is_displayed()

    def click_on_logout_button(self):
        self.driver.find_element(By.XPATH, self.my_account_option_path).click()
        self.driver.find_element(By.XPATH, self.logout_option_message_path).click()