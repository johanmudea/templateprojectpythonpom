from selenium.webdriver.common.by import By


class HomePage:
    def __init__(self, driver):
        self.driver = driver


    my_account_option_path = "//a[contains(@class,'dropdown-toggle') and contains(@title,'My Account') ]"
    register_option_path = "//*[@id='top-links']/ul/li[2]/ul/li[1]/a"
    login_option_path = "//*[@id='top-links']/ul/li[2]/ul/li[2]/a"

    def click_on_my_account(self):
        self.driver.find_element(By.XPATH,self.my_account_option_path).click()


    def click_on_register(self):
      self.driver.find_element(By.XPATH, self.register_option_path).click()

    def click_on_login(self):
      self.driver.find_element(By.XPATH, self.login_option_path).click()

