from selenium.webdriver.common.by import By

class LoginPage:
    def __init__(self, driver):
        self.driver = driver

    email_path = "input-email"
    password_path = "input-password"
    login_button_path = "//input[contains(@type,'submit') ]"
    expected_Warning_path = '//*[@id="account-login"]/div[1]'

    def enter_valid_email(self):
        self.driver.find_element(By.ID, self.email_path).send_keys("arpsoft273@gmail.com")
    def enter_valid_password(self):
        self.driver.find_element(By.ID, self.password_path).send_keys("arpsoft273")

    def enter_invalid_email(self):
        self.driver.find_element(By.ID, self.email_path).send_keys("arpsoft@gmail.com")

    def enter_invalid_password(self):
        self.driver.find_element(By.ID, self.password_path).send_keys("arpsoft27345")

    def click_on_login_button(self):
      self.driver.find_element(By.XPATH, self.login_button_path).click()

    def click_on_email(self):
        self.driver.find_element(By.ID, self.email_path).click()
    def click_on_password(self):
        self.driver.find_element(By.ID, self.password_path).click()


    def display_status_of_warning_message(self, expected_Warning):
        return self.driver.find_element(By.XPATH, self.expected_Warning_path).text.__contains__(expected_Warning)

