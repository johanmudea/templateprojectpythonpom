import time

from faker import Faker
from selenium.webdriver.common.by import By

from templateprojectpythonpom.features.pages.AccountPage import AccountPage
from templateprojectpythonpom.features.pages.HomePage import HomePage


class RegisterPage:

    def __init__(self, driver):
        self.driver = driver

    first_name_path = "input-firstname"
    last_name_path = "input-lastname"
    email_path = "input-email"
    telephone_path = "input-telephone"
    password_path = "input-password"
    confirmation_password_path = "input-confirm"
    newsletter_subscription_path = "//input[@value='0']"
    privacy_policy_path = "//input[@type='checkbox']"
    continue_button_path = "//input[@type='submit']"
    expected_created_message_path = '//*[@id="content"]/h1'
    expected_warning_message_path = '//body/div[2]/div[1]'
    expected_privacy_warning_message_path = '//*[@id="account-register"]/div[1]'

    def fill_mandatory_fields(self):
        fake = Faker()
        password = fake.password()
        self.driver.find_element(By.ID, self.first_name_path).send_keys(fake.name())
        self.driver.find_element(By.ID, self.last_name_path).send_keys(fake.last_name())
        self.driver.find_element(By.ID, self.email_path).send_keys(fake.email())
        self.driver.find_element(By.ID, self.telephone_path).send_keys(fake.phone_number())
        self.driver.find_element(By.ID, self.password_path).send_keys(password)
        self.driver.find_element(By.ID, self.confirmation_password_path).send_keys(password)
    def fill_all_fields(self):
        fake = Faker()
        password = fake.password()
        self.driver.find_element(By.ID, self.first_name_path).send_keys(fake.name())
        self.driver.find_element(By.ID, self.last_name_path).send_keys(fake.last_name())
        self.driver.find_element(By.ID, self.email_path).send_keys(fake.email())
        self.driver.find_element(By.ID, self.telephone_path).send_keys(fake.phone_number())
        self.driver.find_element(By.ID, self.password_path).send_keys(password)
        self.driver.find_element(By.ID, self.confirmation_password_path).send_keys(password)
        self.driver.find_element(By.XPATH, self.newsletter_subscription_path).click()
    def fill_all_fields_except_email(self):
        fake = Faker()
        password = fake.password()
        self.driver.find_element(By.ID, self.first_name_path).send_keys(fake.name())
        self.driver.find_element(By.ID, self.last_name_path).send_keys(fake.last_name())
        #self.driver.find_element(By.ID, self.email_path).send_keys(fake.email())
        self.driver.find_element(By.ID, self.telephone_path).send_keys(fake.phone_number())
        self.driver.find_element(By.ID, self.password_path).send_keys(password)
        self.driver.find_element(By.ID, self.confirmation_password_path).send_keys(password)
        self.driver.find_element(By.XPATH, self.newsletter_subscription_path).click()
    def fill_all_fields_using_already_existing_email(self):

        fake = Faker()
        account_page = AccountPage(self.driver)
        home_page = HomePage(self.driver)
        email = fake.email()
        password = fake.password()
        self.driver.find_element(By.ID, self.email_path).send_keys(email)
        self.driver.find_element(By.XPATH, self.newsletter_subscription_path).click()
        self.driver.find_element(By.XPATH, self.privacy_policy_path).click()
        self.driver.find_element(By.XPATH, self.continue_button_path).click()
        time.sleep(1)

        #self.driver.find_element(By.XPATH, "//a[contains(@class,'dropdown-toggle') and contains(@title,'My Account') ]").click()
        #self.driver.find_element(By.XPATH, '//*[@id="top-links"]/ul/li[2]/ul/li[5]/a').click()
        account_page.click_on_logout_button()
        time.sleep(2)

        #self.driver.find_element(By.XPATH, "//a[contains(@class,'dropdown-toggle') and contains(@title,'My Account') ]").click()
        #self.driver.find_element(By.XPATH, "//*[@id='top-links']/ul/li[2]/ul/li[1]/a").click()
        home_page.click_on_my_account()
        home_page.click_on_register()

        time.sleep(1)

        self.driver.find_element(By.ID, self.first_name_path).send_keys(fake.name())
        self.driver.find_element(By.ID, self.last_name_path).send_keys(fake.last_name())
        self.driver.find_element(By.ID, self.email_path).send_keys(email)
        self.driver.find_element(By.ID, self.telephone_path).send_keys(fake.phone_number())
        self.driver.find_element(By.ID, self.password_path).send_keys(password)
        self.driver.find_element(By.ID, self.confirmation_password_path).send_keys(password)
        self.driver.find_element(By.XPATH, self.newsletter_subscription_path).click()
        self.driver.find_element(By.XPATH, self.privacy_policy_path).click()
        self.driver.find_element(By.XPATH, self.continue_button_path).click()
        time.sleep(1)
    def click_on_privacy_policy_check(self):
      self.driver.find_element(By.XPATH, self.privacy_policy_path).click()
    def click_on_continue_button(self):
      self.driver.find_element(By.XPATH, self.continue_button_path).click()
    def display_expected_created_message(self, expected_created_message):
        return self.driver.find_element(By.XPATH, self.expected_created_message_path).text.__contains__(expected_created_message)

    def display_expected_warning_message(self, expected_warning_message):
        return self.driver.find_element(By.XPATH, self.expected_warning_message_path).text.__contains__(expected_warning_message)

    def display_expected_privacy_warning_message(self, expected_privacy_warning_message):
        return self.driver.find_element(By.XPATH, self.expected_privacy_warning_message_path).text.__eq__(expected_privacy_warning_message)