Feature: Login Functionality


  @pom
  Scenario: Login with valid credentials
    Given I navigated to login page
    When I enter valid email address and  valid password into the fields
    And I click on login button
    Then I should get logged in

  @ignore
  Scenario: Login with invalid email and valid password
    Given I navigated to login page
    When I enter invalid email address and  valid password into the fields
    And I click on login button
    Then I should get a proper warning message

  @ignore
  Scenario: Login with valid email and invalid password
    Given I navigated to login page
    When I enter valid email address and  invalid password into the fields
    And I click on login button
    Then I should get a proper warning message

  @ignore
  Scenario: Login with invalid credentials
    Given I navigated to login page
    When I enter invalid email address and invalid password into the fields
    And I click on login button
    Then I should get a proper warning message

  @completed
  Scenario: Login with entering any credentials
    Given I navigated to login page
    When I dont enter anything into fields
    And I click on login button
    Then I should get a proper warning message