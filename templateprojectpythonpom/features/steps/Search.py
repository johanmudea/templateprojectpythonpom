import time

from behave import *
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By

from webdriver_manager.chrome import ChromeDriverManager

from templateprojectpythonpom.features.pages.SearchPage import SearchPage


@given(u'I got navigated to home page')
def step_impl(context):
    print("Inside -> II got navigated to home page")

    expected_title = "Your Store"

@when(u'I enter valid product into the search box field')
def step_impl(context):
    print("Inside -> I enter valid product into the search box field")

    search_page = SearchPage(context.driver)
    search_page.enter_search_text("HP")
    #context.driver.find_element(By.NAME, "search").click()
    #context.driver.find_element(By.NAME, "search").send_keys("HP")


@when(u'I click on search button')
def step_impl(context):
    print("Inside -> I click on search button")

    search_page = SearchPage(context.driver)
    search_page.click_on_search_button()
    #context.driver.find_element(By.XPATH, "//button[contains(@class,'btn btn-default btn-lg' )]").click()

@then(u'Valid product shoud get displayed in search results')
def step_impl(context):
    print("Inside -> Valid product shoud get displayed in search results")

    search_page = SearchPage(context.driver)
    assert search_page.display_status_of_searched_product()
    #assert context.driver.find_element(By.LINK_TEXT, "HP LP3065").is_displayed()

@when(u'I enter invalid product into the search box field')
def step_impl(context):
    print("Inside -> I enter invalid product into the search box field")

    search_page = SearchPage(context.driver)
    search_page.enter_search_text("HONDA")


    #context.driver.find_element(By.NAME, "search").click()
    #context.driver.find_element(By.NAME, "search").send_keys("HONDA")

@then(u'Proper message  shoud get displayed in search results')
def step_impl(context):
    print("Inside -> Proper message  shoud get displayed in search results")

    search_page = SearchPage(context.driver)

    expected_Warning = "There is no product that matches the search criteria."
    """
    obtainedText = context.driver.find_element(By.XPATH, "//*[@id='content']/p[2]").text
    assert obtainedText == expected_Warning
    """
    #assert context.driver.find_element(By.XPATH, "//*[@id='content']/p[2]").text.__eq__(expected_Warning)
    assert search_page.display_proper_message(expected_Warning)

@when(u'I dont enter anything into search box field')
def step_impl(context):
    print("Inside -> I dont enter anything into search box field")

    search_page = SearchPage(context.driver)
    search_page.enter_search_text("")



