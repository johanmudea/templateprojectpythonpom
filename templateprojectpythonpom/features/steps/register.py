import time
from behave import *
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from faker import Faker

from templateprojectpythonpom.features.pages.HomePage import HomePage
from templateprojectpythonpom.features.pages.RegisterPage import RegisterPage


@given(u'I navigate to Register Page')
def step_impl(context):
    print("Inside -> I navigate to Register Page")

    #chrome_options = Options()
    #chrome_options.add_argument('--incognito')  # Inicializa incognito
    #chrome_options.add_argument('--start-maximized')  # Maximiza la ventana
    #serviceChrome = Service(ChromeDriverManager().install())
    #context.driver = webdriver.Chrome(service=serviceChrome, options=chrome_options)
    #context.driver.get("https://tutorialsninja.com/demo/")

    #context.driver.find_element(By.XPATH, "//a[contains(@class,'dropdown-toggle') and contains(@title,'My Account') ]").click()
    #context.driver.find_element(By.XPATH, "//*[@id='top-links']/ul/li[2]/ul/li[1]/a").click()

    home_page = HomePage(context.driver)
    home_page.click_on_my_account()
    home_page.click_on_register()


@when(u'I enter below details into mandatory fields')
def step_impl(context):
    print("Inside -> I enter below details into mandatory fields")

    #fake = Faker()
    #password = fake.password()
    #context.driver.find_element(By.ID, "input-firstname").send_keys(fake.name())
    #context.driver.find_element(By.ID, "input-lastname").send_keys(fake.last_name())
    #context.driver.find_element(By.ID, "input-email").send_keys(fake.email())
    #context.driver.find_element(By.ID, "input-telephone").send_keys(fake.phone_number())
    #context.driver.find_element(By.ID, "input-password").send_keys(password)
    #context.driver.find_element(By.ID, "input-confirm").send_keys(password)

    register_page = RegisterPage(context.driver)
    register_page.fill_mandatory_fields()




@when(u'I select Privacy Policy option')
def step_impl(context):
    print("Inside -> I select Privacy Policy option")

    #context.driver.find_element(By.XPATH, "//input[@type='checkbox']").click()

    register_page = RegisterPage(context.driver)
    register_page.click_on_privacy_policy_check()



@when(u'I click on Continue button')
def step_impl(context):
    print("Inside -> I click on Continue button")

    #context.driver.find_element(By.XPATH, "//input[@type='submit']").click()

    register_page = RegisterPage(context.driver)
    register_page.click_on_continue_button()


@then(u'Account should get created')
def step_impl(context):
    print("Inside -> Account should get created")

    expected_created_message = "Your Account Has Been Created!"
    register_page = RegisterPage(context.driver)
    assert register_page.display_expected_created_message(expected_created_message)

    #assert context.driver.find_element(By.XPATH, '//*[@id="content"]/h1').text.__eq__(expected_created_message)




@when(u'I enter below details into all fields')
def step_impl(context):
    print("Inside -> I enter below details into all fields")

    fake = Faker()
    password = fake.password()
    context.driver.find_element(By.ID, "input-firstname").send_keys(fake.name())
    context.driver.find_element(By.ID, "input-lastname").send_keys(fake.last_name())
    context.driver.find_element(By.ID, "input-email").send_keys(fake.email())
    context.driver.find_element(By.ID, "input-telephone").send_keys(fake.phone_number())
    context.driver.find_element(By.ID, "input-password").send_keys(password)
    context.driver.find_element(By.ID, "input-confirm").send_keys(password)
    context.driver.find_element(By.XPATH, "//input[@value='0']").click()



@when(u'I enter details into all fields except email field')
def step_impl(context):
    print("Inside -> I enter details into all fields except email field")
    fake = Faker()
    password = fake.password()
    context.driver.find_element(By.ID, "input-firstname").send_keys(fake.name())
    context.driver.find_element(By.ID, "input-lastname").send_keys(fake.last_name())
    #context.driver.find_element(By.ID, "input-email").send_keys(fake.email())
    context.driver.find_element(By.ID, "input-telephone").send_keys(fake.phone_number())
    context.driver.find_element(By.ID, "input-password").send_keys(password)
    context.driver.find_element(By.ID, "input-confirm").send_keys(password)
    context.driver.find_element(By.XPATH, "//input[@value='0']").click()
    time.sleep(5)


@when(u'I enter existing accounts email into email field')
def step_impl(context):
    print("Inside -> I enter existing accounts email into email field")

    register_page = RegisterPage(context.driver)
    register_page.fill_all_fields_using_already_existing_email()



"""    fake = Faker()
    email = fake.email()
    password = fake.password()
    #context.driver.find_element(By.ID, "input-firstname").send_keys(fake.name())
    #context.driver.find_element(By.ID, "input-lastname").send_keys(fake.last_name())
    context.driver.find_element(By.ID, "input-email").send_keys(email)
    #context.driver.find_element(By.ID, "input-telephone").send_keys(fake.phone_number())
    #context.driver.find_element(By.ID, "input-password").send_keys(password)
    #context.driver.find_element(By.ID, "input-confirm").send_keys(password)
    context.driver.find_element(By.XPATH, "//input[@value='0']").click()
    context.driver.find_element(By.XPATH, "//input[@type='checkbox']").click()
    context.driver.find_element(By.XPATH, "//input[@type='submit']").click()
    time.sleep(2)

    context.driver.find_element(By.XPATH, "//a[contains(@class,'dropdown-toggle') and contains(@title,'My Account') ]").click()
    context.driver.find_element(By.XPATH, '//*[@id="top-links"]/ul/li[2]/ul/li[5]/a').click()
    time.sleep(2)

    context.driver.find_element(By.XPATH, "//a[contains(@class,'dropdown-toggle') and contains(@title,'My Account') ]").click()
    context.driver.find_element(By.XPATH, "//*[@id='top-links']/ul/li[2]/ul/li[1]/a").click()
    time.sleep(2)

    context.driver.find_element(By.ID, "input-firstname").send_keys(fake.name())
    context.driver.find_element(By.ID, "input-lastname").send_keys(fake.last_name())
    context.driver.find_element(By.ID, "input-email").send_keys(email)
    context.driver.find_element(By.ID, "input-telephone").send_keys(fake.phone_number())
    context.driver.find_element(By.ID, "input-password").send_keys(password)
    context.driver.find_element(By.ID, "input-confirm").send_keys(password)
    context.driver.find_element(By.XPATH, "//input[@value='0']").click()
    context.driver.find_element(By.XPATH, "//input[@type='checkbox']").click()
    context.driver.find_element(By.XPATH, "//input[@type='submit']").click()
    time.sleep(1)
"""


@then(u'Proper warning message informing about duplicate account should be displayed')
def step_impl(context):
    print("Inside -> Proper warning message informing about duplicate account should be displayed")

    register_page = RegisterPage(context.driver)
    expected_warning_message = "Warning"
    assert register_page.display_expected_warning_message(expected_warning_message)


    #assert context.driver.find_element(By.XPATH, '//body/div[2]/div[1]').text.__contains__(expected_warning_message)

@when(u'I dont enter anything into the fields')
def step_impl(context):
    print("Inside -> I dont enter anything into the fields")

    register_page = RegisterPage(context.driver)

    #context.driver.find_element(By.XPATH, "//input[@value='0']").click()
    #context.driver.find_element(By.XPATH, "//input[@type='submit']").click()


@then(u'Proper warning messages for every mandatory fields should be displayed')
def step_impl(context):
    print("Inside -> Proper warning messages for every mandatory fields should be displayed")

    register_page = RegisterPage(context.driver)
    expected_privacy_warning_message = "Warning: You must agree to the Privacy Policy!"
    assert register_page.display_expected_privacy_warning_message(expected_privacy_warning_message)

    #assert context.driver.find_element(By.XPATH, '//*[@id="account-register"]/div[1]').text.__eq__()
