import time
from behave import *
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

from templateprojectpythonpom.features.pages.AccountPage import AccountPage
from templateprojectpythonpom.features.pages.HomePage import HomePage
from templateprojectpythonpom.features.pages.LoginPage import LoginPage


@given(u'I navigated to login page')
def step_impl(context):
    print("Inside -> I navigated to login page")

    home_page = HomePage(context.driver)
    home_page.click_on_my_account()
    home_page.click_on_login()

    #context.driver.find_element(By.XPATH, "//a[contains(@class,'dropdown-toggle') and contains(@title,'My Account') ]").click()
    #context.driver.find_element(By.XPATH, "//*[@id='top-links']/ul/li[2]/ul/li[2]/a").click()



@when(u'I enter valid email address and  valid password into the fields')
def step_impl(context):
    print("Inside ->I enter valid email address and  valid password into the fields")

    login_page = LoginPage(context.driver)
    login_page.enter_valid_email()
    login_page.enter_valid_password()



    #context.driver.find_element(By.ID, "input-email").send_keys("arpsoft273@gmail.com")
    #context.driver.find_element(By.ID, "input-password").send_keys("arpsoft273")


@when(u'I click on login button')
def step_impl(context):
    print("Inside -> I click on login button")

    login_page = LoginPage(context.driver)
    login_page.click_on_login_button()

    #context.driver.find_element(By.XPATH, "//input[contains(@type,'submit') ]").click()


@then(u'I should get logged in')
def step_impl(context):
    print("Inside -> I should get logged in")

    #context.driver.find_element(By.XPATH, "//a[contains(@class,'dropdown-toggle') and contains(@title,'My Account') ]").click()
    home_page = HomePage(context.driver)
    home_page.click_on_my_account()
    account_page = AccountPage(context.driver)
    assert account_page.display_status_of_logout_your_account_information_option()


    #expectedLogout = "Logout"
    #assert context.driver.find_element(By.XPATH, '//*[@id="top-links"]/ul/li[2]/ul/li[5]/a').text.__eq__(expectedLogout)
    time.sleep(2)



@when(u'I enter invalid email address and  valid password into the fields')
def step_impl(context):
    print("Inside -> I enter invalid email address and  valid password into the fields")

    login_page = LoginPage(context.driver)
    login_page.enter_invalid_email()
    login_page.enter_valid_password()

    #context.driver.find_element(By.ID, "input-email").send_keys("arpsoft@gmail.com")
    #context.driver.find_element(By.ID, "input-password").send_keys("arpsoft273")


@then(u'I should get a proper warning message')
def step_impl(context):
    print("Inside -> I should get a proper warning message")

    login_page = LoginPage(context.driver)
    expected_Warning = "Warning: No match for E-Mail Address and/or Password."
    assert login_page.display_status_of_warning_message(expected_Warning)





@when(u'I enter valid email address and  invalid password into the fields')
def step_impl(context):
    print("Inside -> I enter valid email address and  invalid password into the fields")

    login_page = LoginPage(context.driver)
    login_page.enter_valid_email()
    login_page.enter_invalid_password()

    #context.driver.find_element(By.ID, "input-email").send_keys("arpsoft273@gmail.com")
    #context.driver.find_element(By.ID, "input-password").send_keys("arpsoft27345")


@when(u'I enter invalid email address and invalid password into the fields')
def step_impl(context):
    print("Inside -> I enter invalid email address and invalid password into the fields")

    login_page = LoginPage(context.driver)
    login_page.enter_invalid_email()
    login_page.enter_invalid_password()

    #context.driver.find_element(By.ID, "input-email").send_keys("arpsoft27345@gmail.com")
    #context.driver.find_element(By.ID, "input-password").send_keys("arpsoft27345")

@when(u'I dont enter anything into fields')
def step_impl(context):
    print("Inside -> I dont enter anything into fields")

    login_page = LoginPage(context.driver)
    login_page.click_on_email()
    login_page.click_on_email()

    #context.driver.find_element(By.ID, "input-email").click()
    #context.driver.find_element(By.ID, "input-password").click()

