Feature: Search functionality

      @search1
    Scenario: Search for a valid product
    Given I got navigated to home page
    When I enter valid product into the search box field
    And I click on search button
    Then Valid product shoud get displayed in search results

      @search2
    Scenario: Search for a Invalid product
      Given I got navigated to home page
      When I enter invalid product into the search box field
      And I click on search button
      Then Proper message  shoud get displayed in search results

      @search3
    Scenario: Search without entering any product
      Given I got navigated to home page
      When I dont enter anything into search box field
      And I click on search button
      Then Proper message  shoud get displayed in search results
